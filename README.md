# Unity plugin for folder description.

The plugin is designed to add information to folders. You can add text, images, hyperlinks, as well as the history of changes.

When you select a folder in the project, the inspector will show the description of the folder (if any) and 2 buttons.

**Blame Button.** To see the history of changes, you need to click the Blame button. By clicking in the inspector window, a history of changes for a given period will appear (the period can be configured in the options).

**Edit description Button.** When you click on the button, a description editing window appears; you can add or edit the description of the folder.

You can write text, it will just be displayed, as well as add tags.
Valid tags:

**[caption: text]** - adds the title “text” in capital letters.

**[separator]** - adds a gap between the text.

**[image / smallimage / bigimage: guid]** - adding a picture with id “Guid”.

**[url: link]** - adds a clickable hyperlink “link”.

**[box / infobox / warningbox / errorbox: text]** - adds the text “text” in the frame.

**[richtext: text]** - adds the text "text" into which you can add richtext tags.

**[history: time_ago]** - adds the history of changes for a given period.

**[blamebutton: time_ago]** - similar to the previous tag, adds a button when clicked which shows the history of changes for a given period.


## Settings are in Unity-> Preferences ... -> Folder Description Settings.

**Blame Command** - you can edit the git command to get the history of changes, for example, you can add the key ‘—merges’ to see only merge changes. [0] - timeAgo, [1] - folder path

**Repository Url** - a link that opens when you click on a selected line in the change history. At the end, a commit key is added. If not set, then nothing opens on a click.

**Default Time Ago** - sets the default time for the Blame button.
