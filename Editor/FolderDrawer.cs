﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace FolderDescription.Editor
{
    [CustomEditor(typeof(DefaultAsset))]
    public class FolderDrawer : UnityEditor.Editor
    {
        private enum _FolderDrawerStates
        {
            Unknown,
            Active,
            NoDescription
        }

		private _FolderDrawerStates _state = _FolderDrawerStates.Unknown;
		private List<IDrawer> _drawers = new List<IDrawer>();

		private readonly Dictionary<string, Func<string, IDrawer>> _supportedDrawers = new Dictionary<string, Func<string, IDrawer>>()
        {
            { "image", (arg) => new FolderImage(arg)},
            { "smallimage", (arg) => new FolderImage(arg) { ImageSize = Settings.DefaultSmallImageSize }},
            { "bigimage", (arg) => new FolderImage(arg) { ImageSize = Settings.DefaultBigImageSize }},
            { "url", (arg) => new FolderLink(arg)},
            { "caption", (arg) => new FolderCaption(arg)},
            { "separator", (arg) => new FolderSeparator(arg)},
            { "richtext", (arg) => new FolderDefault(arg) { RichText = true }},
            { "history", (arg) => new FolderHistory(arg)},
            { "blamebutton", (arg) => new FolderHistory(arg) { BlameButton = true }},
            { "box", (arg) => new FolderInfoBox(arg) { Type = MessageType.None }},
            { "infobox", (arg) => new FolderInfoBox(arg) { Type = MessageType.Info }},
            { "warningbox", (arg) => new FolderInfoBox(arg) { Type = MessageType.Warning }},
			{ "errorbox", (arg) => new FolderInfoBox(arg) { Type = MessageType.Error }},
            // add new drawers here, ...
        };

        public override void OnInspectorGUI()
        {
            if (_state == _FolderDrawerStates.Unknown)
            {
                Init();
            }

            Draw(_state == _FolderDrawerStates.Active);
        }

        private void Reset()
        {
            _state = _FolderDrawerStates.Unknown;
            _drawers.Clear();
            Repaint();
        }

        private void Blame()
        {
            _drawers.Clear();
            Parse("[history:" + Settings.DefaultTimeAgo + "]");
            _state = _FolderDrawerStates.Active;
            Repaint();
        }

        private void Init()
        {
            var folderDescription = LoadData(Selection.activeObject);
            if (string.IsNullOrEmpty(folderDescription))
            {
                _state = _FolderDrawerStates.NoDescription;
                return;
            }

            Parse(folderDescription);
            _state = _FolderDrawerStates.Active;
        }

        private void Draw(bool hasDescription)
        {
            var guiState = GUI.enabled;
            GUI.enabled = true;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Blame", EditorStyles.miniButton))
            {
                Blame();
            }

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Edit description...", EditorStyles.miniButton))
            {
                EditFolderDescription(() => Reset());
            }

            GUILayout.EndHorizontal();
            if (hasDescription)
            {
                foreach (var dr in _drawers)
                {
                    dr.Draw();
                }
            }

            GUI.enabled = guiState;
        }

        private void EditFolderDescription(Action onDescriptionSaved)
        {
            var path = AssetDatabase.GetAssetPath(Selection.activeObject);
            FolderDescriptionEditor.Init(path, onDescriptionSaved);
        }

        private string LoadData(UnityEngine.Object folder)
        {
            var path = AssetDatabase.GetAssetPath(folder);
            var import = AssetImporter.GetAtPath(path);
            return import.userData;
        }

        private void Parse(string folderDescription)
        {
            var taggedStrings = new List<string>();
            var dataStrings = Regex.Split(folderDescription, Settings.RegexPattern, RegexOptions.Singleline | RegexOptions.Multiline);
            var matches = Regex.Matches(folderDescription, Settings.RegexPattern, RegexOptions.Singleline | RegexOptions.Multiline);

            foreach (Match match in matches)
            {
                taggedStrings.Add(match.Groups[1].Value);
            }

            foreach (var st in dataStrings)
            {
                if (string.IsNullOrEmpty(st)) continue;

                if (taggedStrings.Contains(st))
                {
                    taggedStrings.Remove(st);

                    var splitter = st.Split(new char[] { ':' }, 2);
                    var tag = splitter[0];
                    if (_supportedDrawers.ContainsKey(tag))
                    {
                        var drawer = _supportedDrawers[tag].Invoke(
                            splitter.Length > 1
                                ? splitter[1]
                                : splitter[0]);
                        _drawers.Add(drawer);
                    }
                    else
                    {
                        _drawers.Add(new FolderUnknownTag(st));
                    }

                    continue;
                }

                var defaultDrawer = new FolderDefault(st);
                _drawers.Add(defaultDrawer);
            }
        }
    }
}
