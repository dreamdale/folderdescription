﻿using System;
using UnityEngine;
using UnityEditor;

namespace FolderDescription.Editor
{
    public class FolderDescriptionEditor : EditorWindow
    {
        private Action OnDescriptionSaved;
        private string _data;
        private string _path;

        public static void Init(string path, Action onDescriptionSaved = null)
        {
            var window = CreateInstance<FolderDescriptionEditor>();
            window.SetUp(path, onDescriptionSaved);
            window.Show();
        }

        private void SetUp(string path, Action onDescriptionSaved = null)
        {
            var import = AssetImporter.GetAtPath(path);
            _path = path;
            _data = import.userData;
            minSize = Settings.MinWindowSize;
            OnDescriptionSaved = onDescriptionSaved;
        }

        private void OnGUI()
        {
            EditorGUILayout.HelpBox(Settings.HelpText, MessageType.Info);
            _data = GUILayout.TextArea(_data, GUILayout.Height(position.height - Settings.BottomOffset));
            if (GUILayout.Button("Save and close"))
            {
                Save();
                if (OnDescriptionSaved != null)
                {
                    OnDescriptionSaved.Invoke();
                }

                Close();
            }
        }

        private void Save()
        {
            var import = AssetImporter.GetAtPath(_path);
            import.userData = _data;
            import.SaveAndReimport();
        }
    }
}
