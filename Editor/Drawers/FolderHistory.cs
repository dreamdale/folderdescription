﻿using UnityEditor;
using UnityEngine;
using System;
using System.Diagnostics;
using System.Linq;

namespace FolderDescription.Editor
{
    internal class FolderHistory : IDrawer
    {
        public bool BlameButton = false;

        public bool Clickable
        {
            get { return !string.IsNullOrEmpty(Settings.RepositoryUrl); }
        }
        private readonly string _timeAgo;
        private readonly GUIStyle _historyStyle;
        private string _text;
        private string[] _textLines;

        public FolderHistory(string arg)
        {
            _timeAgo = arg;

            _historyStyle = new GUIStyle(EditorStyles.label)
            {
                stretchWidth = false
            };

            if (!BlameButton)
            {
                Blame();
            }
        }

        public void Draw()
        {
            if (BlameButton)
            {
                ShowBlameButton();
            }
            else
            {
                ShowHistory();
            }
        }

        private void ShowBlameButton()
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Blame Folder History", EditorStyles.miniButton))
            {
                Blame();
                BlameButton = false;
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void Blame()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            var commandArgs = string.Format(Settings.BlameCommand, _timeAgo, path);
            using (var process = ExecuteShellCommand("git", commandArgs))
            {
                _text = process.StandardOutput.ReadToEnd();
            }

            _textLines = _text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        private void ShowHistory()
        {
            if (!string.IsNullOrEmpty(_text))
            {
                GUILayout.Label("Last changes since " + _timeAgo, EditorStyles.boldLabel);
                if (Clickable)
                {
                    foreach (var line in _textLines)
                        if (GUILayout.Button(line, _historyStyle, GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth - Settings.RightOffset)))
                        {
                            EditorGUIUtility.systemCopyBuffer = line;
                            var splitted = line.Split('\t');
                            Help.BrowseURL(Settings.RepositoryUrl + splitted.FirstOrDefault());
                        }
                }
                else
                {
                    GUILayout.Label(_text, _historyStyle, GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth - Settings.RightOffset));
                }
            }
            else
            {
                GUILayout.Label("No changes since " + _timeAgo, EditorStyles.boldLabel);
            }
        }

        private static Process ExecuteShellCommand(string command,
                                                   string args,
                                                   string path = null,
                                                   bool useShellExecute = false)
        {
            var processInfo = new ProcessStartInfo()
            {
                FileName = command,
                UseShellExecute = useShellExecute,
                RedirectStandardOutput = !useShellExecute,
                RedirectStandardError = !useShellExecute,
                Arguments = args,
                WorkingDirectory = path
            };

            var process = Process.Start(processInfo);
            if (process == null)
            {
                throw new Exception("Failed to execute command '" + command + " " + args + "'. Process not started.");
            }

            process.WaitForExit();

            return process;
        }
    }
}
