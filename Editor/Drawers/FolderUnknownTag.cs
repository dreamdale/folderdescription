﻿using UnityEditor;
using UnityEngine;

namespace FolderDescription.Editor
{
    internal class FolderUnknownTag : IDrawer
    {
        private readonly string _text;
        private readonly GUIStyle _boldStyle;

        public FolderUnknownTag(string arg)
        {
            _text = arg;
            _boldStyle = new GUIStyle(EditorStyles.boldLabel)
            {
                normal = {textColor = Color.red}
            };
        }

        public void Draw()
        {
            GUILayout.Label("Unknown tag : " + _text, _boldStyle);
        }
    }
}
