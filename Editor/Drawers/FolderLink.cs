﻿using UnityEngine;
using UnityEditor;

namespace FolderDescription.Editor
{
    internal class FolderLink : IDrawer
    {
        private readonly string _url;
        private readonly GUIStyle _linkStyle;

        public FolderLink(string arg)
        {
            _url = arg;
            _linkStyle = new GUIStyle(EditorStyles.label)
            {
                wordWrap = false, 
                normal = {textColor = Settings.LinkColor}, 
                stretchWidth = false
            };
        }

        public void Draw()
        {
            if (LinkLabel(new GUIContent(_url), GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth - Settings.RightOffset)))
            {
                Application.OpenURL(_url);
            }
        }

        private bool LinkLabel(GUIContent label, params GUILayoutOption[] options)
        {
            var position = GUILayoutUtility.GetRect(label, _linkStyle, options);
            Handles.BeginGUI();
            Handles.color = _linkStyle.normal.textColor;
            Handles.DrawLine(new Vector3(position.xMin, position.yMax), new Vector3(position.xMax, position.yMax));
            Handles.color = Color.white;
            Handles.EndGUI();

            EditorGUIUtility.AddCursorRect(position, MouseCursor.Link);
            return GUI.Button(position, label, _linkStyle);
        }
    }
}
