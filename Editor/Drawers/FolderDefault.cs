﻿using UnityEditor;
using UnityEngine;

namespace FolderDescription.Editor
{
    internal class FolderDefault : IDrawer
    {
        public bool RichText
        {
            get { return _guiStyle.richText; }
            set { _guiStyle.richText = value; }
        }

        public int FontSize
        {
            get { return _guiStyle.fontSize; }
            set { _guiStyle.fontSize = value; }
        }

        public FontStyle FontStyle
        {
            get { return _guiStyle.fontStyle; }
            set { _guiStyle.fontStyle = value; }
        }

        public Color Color
        {
            get { return _guiStyle.normal.textColor; }
            set { _guiStyle.normal.textColor = value; }
        }

        private readonly string _text;
        private readonly GUIStyle _guiStyle = new GUIStyle(EditorStyles.label);

        public FolderDefault(string arg)
        {
            _text = arg;
        }

        public void Draw()
        {
            GUILayout.Label(_text, _guiStyle);
        }
    }
}
