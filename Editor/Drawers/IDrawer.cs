﻿namespace FolderDescription.Editor
{
    internal interface IDrawer
    {
        void Draw();
    }
}
