﻿using UnityEditor;

namespace FolderDescription.Editor
{
    internal class FolderSeparator : IDrawer
    {
        public FolderSeparator(string arg)
        {
        }

        public void Draw()
        {
            EditorGUILayout.Separator();
        }
    }
}
