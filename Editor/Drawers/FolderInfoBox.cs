﻿using UnityEditor;

namespace FolderDescription.Editor
{
    internal class FolderInfoBox : IDrawer
    {
        public MessageType Type;
        private readonly string _text;

        public FolderInfoBox(string arg)
        {
            _text = arg;
        }

        public void Draw()
        {
            EditorGUILayout.HelpBox(_text, Type);
        }
    }
}
