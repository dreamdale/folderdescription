﻿using UnityEngine;
using UnityEditor;

namespace FolderDescription.Editor
{
    internal class FolderImage : IDrawer
    {
        public float ImageSize = Settings.DefaultImageSize;
        private readonly string _imageGuid;
        private readonly Texture2D _texture;

        public FolderImage(string arg)
        {
            _imageGuid = arg;
            var pathToImage = AssetDatabase.GUIDToAssetPath(_imageGuid);
            if (string.IsNullOrEmpty(pathToImage))
            {
                return;
            }

            _texture = AssetDatabase.LoadAssetAtPath<Texture2D>(pathToImage);
        }

        public void Draw()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (_texture != null)
            {
                var imageWidth = EditorGUIUtility.currentViewWidth * ImageSize;
                var imageHeight = _texture.height * (imageWidth / _texture.width);
                GUI.DrawTexture(GUILayoutUtility.GetRect(imageWidth, imageHeight), _texture);
            }
            else
            {
                GUILayout.Label("Can't load image with id : " + _imageGuid);
            }

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
    }
}
