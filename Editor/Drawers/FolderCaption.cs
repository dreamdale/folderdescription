﻿using UnityEditor;
using UnityEngine;

namespace FolderDescription.Editor
{
    internal class FolderCaption : IDrawer
    {
        public int FontSize = Settings.DefaultCaptionFontSize;
        private readonly string _text;
        private readonly GUIStyle _largeStyle;

        public FolderCaption(string arg)
        {
            _text = arg;
            _largeStyle = new GUIStyle(EditorStyles.label)
            {
                fontSize = FontSize, 
                fontStyle = FontStyle.Bold
            };
        }

        public void Draw()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(_text, _largeStyle);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
    }
}
