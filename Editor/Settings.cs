﻿using UnityEditor;
using UnityEngine;

namespace FolderDescription.Editor {
	public static class Settings {
		#region Common settings
		public const string RegexPattern = @"\[(.*?)\]"; // split by [..]

		public const int RightOffset = 42;
		public const int DefaultCaptionFontSize = 20;
		public const float DefaultImageSize = 0.5f;
		public const float DefaultSmallImageSize = 0.25f;
		public const float DefaultBigImageSize = 0.75f;
		public static Color LinkColor = new Color(0f, 0.47f, 0.85f, 1f);
		#endregion

		#region Blame settings

		public static string BlameCommand {
			get { return EditorPrefs.GetString(BlameCommandTemplateKey, DefaultBlameCommand); }
		}

		public static string RepositoryUrl {
			get { return EditorPrefs.GetString(RepositoryUrlTemplateKey, string.Empty); }
		}

		public static string DefaultTimeAgo {
			get { return EditorPrefs.GetString(DefaultTimeAgoTemplateKey, DefaultDefaultTimeAgo); }
		}

		private const string BlameCommandTemplateKey = "BlameCommand";
		private const string RepositoryUrlTemplateKey = "RepositoryUrl";
		private const string DefaultTimeAgoTemplateKey = "DefaultTimeAgo";
		private const string DefaultBlameCommand = "log --pretty=format:'%h\t- %an,  %ar:\t%s' --since='{0}' -- {1}";
		private const string DefaultDefaultTimeAgo = "3 month ago";
		#endregion

		#region Editor window settings
		public static Vector2 MinWindowSize = new Vector2(400f, 300f);
		public const int BottomOffset = 124;
		public const string HelpText = @"  Supported tags:
[image/smallimage/bigimage:Guid] - add image to description
[url:Url] - add clicable url to description
[caption:Caption] - add caption to description
[separator] - add separator
[richtext:RichText] - add rich text with tags to description
[history/blamebutton:TimeAgo] - blame folder history and add it to description
[box/infobox/warningbox/errorbox:Text] - add box with text to description";
		#endregion

		[PreferenceItem("Folder Description Settings")]
		public static void PreferencesGUI()
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Blame Settings", EditorStyles.boldLabel);

			EditorGUI.BeginChangeCheck();
			var blameCommand = EditorGUILayout.TextField(new GUIContent("Blame Command", "git command\nuse {0} - timeAgo,\n{1} - folder path,\n'--merges' - show only merges"), BlameCommand);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(BlameCommandTemplateKey, blameCommand);
			}
			
			EditorGUI.BeginChangeCheck();
			var repositoryUrl = EditorGUILayout.TextField(new GUIContent("Repository Url", "Please enter here url to repository\nFor example: http://www.myrepository.com/commits/"), RepositoryUrl);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(RepositoryUrlTemplateKey, repositoryUrl);
			}

			EditorGUI.BeginChangeCheck();
			var defaultTimeAgo = EditorGUILayout.TextField(new GUIContent("DefaultTimeAgo", "For example: 3 month ago"), DefaultTimeAgo);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(DefaultTimeAgoTemplateKey, defaultTimeAgo);
			}
		}
	}
}
